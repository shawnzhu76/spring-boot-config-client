package hello;

import org.springframework.beans.factory.annotation.Value;
// import org.springframework.boot.SpringApplication;
// import org.springframework.http.MediaType;
// import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import lombok.extern.slf4j.Slf4j;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


@RestController
@Slf4j
public class ConfigController {
  
  private static final Logger log = LoggerFactory.getLogger(ConfigController.class);
  
  @Value("${spring.datasource.driver-class-name}")
  private String configuredValue;

  @RequestMapping("/config")
  public Config greeting() throws Exception {
    //Info as we don't need to capture this level of details all the time
    log.info("Configured Value: {}", configuredValue);
    try {
        return new Config(configuredValue);
    } catch (Exception ex) {
      //Error level to capture the details of the exception
      log.error(ex.toString());
      throw ex;
    }
  }
}
