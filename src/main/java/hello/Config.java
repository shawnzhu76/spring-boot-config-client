package hello;

public class Config {

    private final String content;

    public Config(String content) {
        this.content = content;
    }

    public String getConfig() {
        return this.content;
    }
}
